> 本项目前端的建立采用 [Create React App](https://marmelab.com/react-admin/Tutorial.html).

BAAS部署为区块链部署带来便利，但是在应用的开发和实施中，仍然需要开发许多区块链应用的基础功能。
这些基础功能包括：账户/密钥管理，区块数据同步与检索，交易的签名与提交等。

Linkel BaseApp(简写为BAR或bar)提供了这些通用的基础功能实现，区块链应用实施者既可以直接复用其提供的功能，
也可以在其源代码的基础上进行开发，快速开发自己的DApp。

![Linkel BaseApp](pics/Linkel%20BaseApp.png)

## 项目介绍
本项目为基于[RepChain](https://gitee.com/BTAJL/repchain) 的区块链应用提供可复用的功能支持，包括：
- 概览：简要信息展示
- 密钥对管理: 数据保存在indexedDB，支持导入导出（所有用户）
- 证书管理：在区块链完成注册的用户数据，通过数据同步提供数据检索和查看（所有用户）
- 账户：在区块链完成注册的用户数据，通过数据同步提供数据检索和查看 （节点管理员可以注册新账号，普通用户只能查看）
- 交易签名和提交：默认向RepChain服务以字节流方式提交签名交易
- 组网管理：即参与p2p组网形成一个链实例，包括创世块建立、信任证书列表维护（平台管理员可以建立组网）
- 节点管理：节点证书管理、节点的入网管理、节点服务启停 （节点管理员可以维护节点）
- 区块管理：从区块链同步数据之后，提供区块检索、查看、验证 （只读检索和查看）
- 交易管理：从区块链同步数据之后，提供交易检索、查看、验证 （只读检索和查看，构造和新建签名交易）

## 如何运行bar
### 依赖环境安装
1. 安装node，推荐使用[nvm](https://github.com/nvm-sh/nvm)安装管理node;
2. 安装yarn，可参考[这里](https://github.com/yarnpkg/yarn);
3. 安装[docker](https://docs.docker.com/install/)以及[docker-compose](https://docs.docker.com/compose/install/)
4. 安装JDK;
5. 安装编译器，如CentOS下可安装gcc-g++
6. 搭建RepChain网络，参考[这里](https://gitee.com/BTAJL/repchain)

### 运行
将本项目git clone到本地之后，运行命令
- `yarn install-all` 安装依赖包(同时安装前端和后端依赖)
- `yarn deploy` 部署Prisma(本应用基于Prisma和MySQL持久化后端数据)
- `yarn start` 同时启动前端和后端服务
> 若需要清空后端数据，可运行: `yarn reset`

## 关于自动化测试选型
- Jest：nodejs单元测试
- Jest + Puppeteer：浏览器端无头测试，集成测试，e2e测试
- Cypress：e2e测试
- jest+enzyme：React组件测试
- 能够进行多浏览真实测试的是：jasmine+karma

以上测试不适用于indexedDB测试，只能进行fake 或 mock，
- 本项目拟采用Jest作为nodejs单元测试，
- 采用Jest+ Puppeteer作为集成测试
- 针对密钥对管理的indexedDB，采用独立的DataProvider以及独立的测试手段