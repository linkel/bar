/*
 * Copyright 2020 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import config from "config";
import RepChainSynchronizer from "repchain-synchronizer";
import { prisma } from "../database/generated/prisma-client";

config.util.setModuleDefaults("bar", config);

export const repChainSynchronizer = new RepChainSynchronizer({
    url: config.get("bar.RepChain.default.url_api"),
    subscribeUrl: config.get("bar.RepChain.default.url_subscribe"),
    prisma,
    logPath: config.get("bar.Log.repchainSynchronizerLogPath"),
});

repChainSynchronizer.startSync();