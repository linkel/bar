# /bin/bash

cd $( dirname ${BASH_SOURCE[0]} )

cd ../database

docker-compose -p bar down

docker volume rm bar_mysql

yarn deploy