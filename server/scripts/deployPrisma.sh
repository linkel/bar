# /bin/bash
cd $( dirname ${BASH_SOURCE[0]} )

npx babel-node generateRSDatamodel

cd ../database

docker-compose -p bar up -d

echo "waiting..."
sleep 20s

npx prisma deploy