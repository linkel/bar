import indexPageTest from "./index";
import accountCreateTest from "./accounts/accountCreate";
import keypairCreateTest from "./keypairs/keypairCreate";
import accountRegisterTest from "./accounts/accountRegister";
import certRegisterTest from "./keypairs/certRegister";
import blockQueryTest from "./blocks/blockQuery";
import transactionQueryTest from "./transactions/transactionQuery";
import transactionSubmitTest from "./transactions/transactionSubmit";

indexPageTest();

accountCreateTest();
keypairCreateTest();
accountRegisterTest();
certRegisterTest();
blockQueryTest();
transactionQueryTest();
transactionSubmitTest();
