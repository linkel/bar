import faker from "faker";
import { setBrowserAndPages, addAccount } from "../helpers/setSharedVariables";
import {
    account1Name,
    account2Name,
    accountListUrl,
    baseUrl,
    accountNodeCreditCode,
    accountNodeName,
} from "../helpers/const";

const accountCreateTest = () => describe("Accounts create test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    // afterAll(() => browser.close());

    it("should direct to the account list page when click the accounts menuitem ", async () => {
        await deskPage.goto(baseUrl);
        await deskPage.waitForSelector("[class^=Menu-main]");
        await deskPage.click("a[role=menuitem][href='#/accounts']");
        expect(deskPage.url()).toBe(accountListUrl);
    });

    it("should direct to account create page when clicking the create button", async () => {
        await deskPage.goto(accountListUrl);
        await deskPage.waitForSelector("a[role=button][aria-label='新建']");
        await deskPage.click("a[role=button][aria-label='新建']");
        expect(deskPage.url()).toBe(`${accountListUrl}/create`);
    });

    const testCreateAccount = (testTitle, {
        name, creditCode, mobile,
    }) => {
        it(testTitle, async () => {
            // create node1 account which is already existed in blockchain
            await deskPage.goto(accountListUrl);
            await deskPage.waitForSelector("a[role=button][aria-label='新建']");
            await deskPage.click("a[role=button][aria-label='新建']");
            const accountCreditCode = creditCode || faker.phone.phoneNumber("##########");
            await deskPage.click("input[name=creditCode]");
            await deskPage.type("input[name=creditCode]", accountCreditCode);
            const accountName = name || faker.name.lastName();
            await deskPage.click("input[name=name]");
            await deskPage.type("input[name=name]", accountName);
            const accountMobile = mobile || faker.phone.phoneNumber("132########");
            await deskPage.click("input[name=mobile]");
            await deskPage.type("input[name=mobile]", accountMobile);
            await deskPage.click("button[type='submit']");

            // show
            await deskPage.waitFor(550);
            await deskPage.waitForSelector("a[aria-label='显示']");
            expect(deskPage.url()).toBe(accountListUrl);
            await deskPage.click("a[aria-label='显示']");
            await expect(deskPage.$eval("#creditCode", (el) => el.innerHTML))
                .resolves.toBe(accountCreditCode);
            await expect(deskPage.$eval("#name", (el) => el.innerHTML))
                .resolves.toBe(accountName);
            await expect(deskPage.$eval("#mobile", (el) => el.innerHTML))
                .resolves.toBe(accountMobile);
            await deskPage.waitForSelector("#status");
            if (accountCreditCode === accountNodeCreditCode) {
                await expect(deskPage.$eval("#status", (el) => el.innerHTML))
                    .resolves.toBe("已注册到区块链");
            } else { 
                await expect(deskPage.$eval("#status", (el) => el.innerHTML))
                    .resolves.toBe("未注册到区块链");
            }

            addAccount({ creditCode: accountCreditCode, name: accountName, mobile: accountMobile });
        }, 5000);
    };

    testCreateAccount("should create and show the node1 account successfully", {
        name: accountNodeName,
        creditCode: accountNodeCreditCode,
    });
    testCreateAccount("should create and show 1st new account successfully", {
        name: account1Name,
    });
    testCreateAccount("should create and show 2nd new account successfully", {
        name: account2Name,
    });
}, 20000);

export default accountCreateTest;
