import { setBrowserAndPages } from "../helpers/setSharedVariables";
import { blockListUrl } from "../helpers/const";

const blockQueryTest = () => describe("Blocks query test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });

    it("should query and view the block list successfully", async () => {
        await deskPage.goto(blockListUrl);
        await deskPage.waitForSelector("a[aria-label='显示']");
        const showButtons = await deskPage.$$("a[aria-label='显示']");
        await deskPage.waitForSelector("[class^=MuiTablePagination] [role=button]");
        const defaultItemCountEachPage = await deskPage.$eval("[class^=MuiTablePagination] [role=button]",
            (el) => parseInt(el.innerHTML, 10));
        expect(defaultItemCountEachPage).toBeGreaterThanOrEqual(showButtons.length);
    });

    it("should query the block list with custom pagination successfully", async () => {
        await deskPage.goto(blockListUrl);
        await deskPage.waitForSelector("[class^=MuiTablePagination] [role=button]");
        await deskPage.click("[class^=MuiTablePagination] [role=button]");
        await deskPage.waitFor(550);
        await deskPage.click("li[data-value='5']");
        await deskPage.waitFor(550);
        await deskPage.waitForSelector("a[aria-label='显示']");
        let showButtons = await deskPage.$$("a[aria-label='显示']");
        await deskPage.waitForSelector("[class^=MuiTablePagination] [role=button]");
        let itemCountEachPage = await deskPage.$eval("[class^=MuiTablePagination] [role=button]",
            (el) => parseInt(el.innerHTML, 10));
        expect(itemCountEachPage).toBeGreaterThanOrEqual(showButtons.length);

        await deskPage.click("button[class$='next-page']");
        await deskPage.waitFor(550);
        await deskPage.waitForSelector("a[aria-label='显示']");
        showButtons = await deskPage.$$("a[aria-label='显示']");
        await deskPage.waitForSelector("[class^=MuiTablePagination] [role=button]");
        itemCountEachPage = await deskPage.$eval("[class^=MuiTablePagination] [role=button]",
            (el) => parseInt(el.innerHTML, 10));
        expect(itemCountEachPage).toBeGreaterThanOrEqual(showButtons.length);
    });

    it("should query and show the block specific info successfully", async () => {
        await deskPage.waitForSelector("a[aria-label='显示']");
        await deskPage.click("a[aria-label='显示']");
        expect(deskPage.url().endsWith("show")).toBeTruthy();
    });
});

export default blockQueryTest;
