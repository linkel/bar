import { random } from "faker";
import { setBrowserAndPages } from "../helpers/setSharedVariables";
import { transactionListUrl, newAccountCount } from "../helpers/const";

const transactionSubmitTest = () => describe("transactions submit test", () => {
    let deskPage;

    beforeAll(async () => {
        const browserAndPages = await setBrowserAndPages();
        deskPage = browserAndPages.page1;
    });
    it("should create and submit a signed transaction successfully", async () => {
        await deskPage.goto(transactionListUrl);
        await deskPage.waitForSelector("a[role=button][aria-label='新建']");
        await deskPage.click("a[role=button][aria-label='新建']");
        expect(deskPage.url()).toBe(`${transactionListUrl}/create`);

        const chaincodeName = "ContractAssetsTPL";
        const chaincodeVersion = "1";
        const chaincodeFunction = "putProof";
        const chaincodeArgs = `{"testKey-${random.uuid()}":"testValue"}`;

        await deskPage.click("#chaincodeName");
        await deskPage.type("#chaincodeName", chaincodeName);
        await deskPage.click("#chaincodeVersion");
        await deskPage.type("#chaincodeVersion", chaincodeVersion);
        await deskPage.click("input[name$=chaincodeFunction]");
        await deskPage.type("input[name$=chaincodeFunction]", chaincodeFunction);
        await deskPage.click("button[class$=chaincodeFunctionArgs]");
        await deskPage.waitFor(550);
        await deskPage.click("[id$=arg]");
        await deskPage.type("[id$=arg]", chaincodeArgs);
        await deskPage.waitFor("label[for=selectAccountName]");
        await deskPage.click("label[for=selectAccountName]");
        const node1AccountOption = (await deskPage.$$("li[class^=MuiButtonBase"))[newAccountCount];
        await node1AccountOption.click();
        await deskPage.waitFor(550);
        await deskPage.click("#selectKeypairName");
        await deskPage.waitFor(550);
        await deskPage.click("li[data-value='1']"); // select node1 keypair to sign
        await deskPage.waitFor(550);
        await deskPage.click("#submitTransaction");

        // verify the submisssion above
        await deskPage.waitFor(3000);
        expect(deskPage.url()).toBe(transactionListUrl);
        await deskPage.reload();
        await deskPage.waitForSelector("a[aria-label='显示']");
        await deskPage.click("a[aria-label='显示']");
        (await deskPage.$$("a[role=tab]"))[1].click();
        await deskPage.waitFor(550);
        await expect(deskPage.$$eval("[class$=chaincodeName] span", (els) => els[1].innerHTML))
            .resolves.toBe(chaincodeName);
        await expect(deskPage.$$eval("[class$=version] span", (els) => els[1].innerHTML))
            .resolves.toBe(chaincodeVersion);
        await expect(deskPage.$$eval("[class$=function] span", (els) => els[1].innerHTML))
            .resolves.toBe(chaincodeFunction);
        // eslint-disable-next-line no-eval
        await expect(deskPage.$$eval("[class$=args] span", (els) => eval(els[1].innerHTML)[0]))
        // eslint-disable-next-line no-eval
            .resolves.toBe(JSON.stringify(eval(`[${chaincodeArgs}]`)[0]));
    }, 15000);
});

export default transactionSubmitTest;
