import puppeteer from "puppeteer-core";
import { baseUrl } from "./const";

export const deskPageViewParams = {
    width: 1920,
    height: 1080,
    deviceScaleFactor: 1,
};

let browser;
let deskPage;
let mobilePage;

export const setBrowserAndPages = async () => {
    const chromiumPath = "/home/zj1nk2iscas/MyApplications/chrome-linux/chrome";
    // const chromiumPath = "/usr/bin/chromium";

    // use singleton
    if (!browser) {
        browser = await puppeteer.launch({
            executablePath: chromiumPath,
            headless: false,
        });
    }
    if (!deskPage) {
        deskPage = await browser.newPage();
        deskPage.setViewport(deskPageViewParams);
        deskPage.goto(baseUrl);
    }
    if (!mobilePage) {
        mobilePage = await browser.newPage();
        mobilePage.emulate(puppeteer.devices["iPhone X"]);
        deskPage.goto(baseUrl);
    }
    return {
        chromium: browser, page1: deskPage, page2: mobilePage,
    };
};

const accounts = [];
export const addAccount = (accountInfo) => {
    accounts.push(accountInfo);
};
export const getAccount = (accountIndex) => accounts[accountIndex];
