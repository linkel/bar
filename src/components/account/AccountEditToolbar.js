import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { GraphQLClient } from "graphql-request";
import {
    SaveButton,
    Toolbar,
} from "react-admin";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import LinearProgress from "@material-ui/core/LinearProgress";
import settings from "../../settings";

const graphqlClient = new GraphQLClient(`${settings.Prisma.endpoint}`);

const styles = (theme) => ({
    button: {
        minHeight: 36,
        marginLeft: 30,
    },
    link: {
        color: "blue",
        marginLeft: 30,
        height: 36,
    },
    buttonIcon: {
        marginRight: "8px",
    },
});

class AccountEditToolbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: null,
        };
    }

    componentDidMount() {
        const { creditCode } = this.props.record;
        graphqlClient.request(`
            {
                account(where: {
                   creditCode: "${creditCode}"
               }){
                   id
               }
            }
        `).then((res) => {
            if (res.account) this.setState({ status: true });
            else this.setState({ status: false });
        });
    }

    render() {
        const { name, creditCode, mobile } = this.props.record;
        const { status } = this.state;
        const { classes } = this.props;
        return (
            <Toolbar {...this.props} >
                <SaveButton redirect="edit" />
                {
                    status === null
                        ? <LinearProgress />
                        : (
                            status
                                ? null
                                : <Button
                                    className={classes.button}
                                    variant="contained"
                                    id="registerAccount"
                                    color="primary"
                                    component={Link}
                                    to={
                                        {
                                            pathname: "/Transaction/create",
                                            state: {
                                                chaincodeName: "ContractCert",
                                                chaincodeVersion: 1,
                                                chaincodeFunction: "SignUpSigner",
                                                chaincodeArgs: [`{"name":"${name}", "creditCode":"${creditCode}", "mobile":"${mobile}", "certNames":[] }`],
                                            },
                                        }
                                    }
                                >
                                    <Icon className={classes.buttonIcon}>person_add</Icon>
                                    注册账号
                                </Button>
                        )
                }
                {
                    status === null
                        ? <LinearProgress />
                        : (status
                            ? <Button
                                className={classes.button}
                                variant="contained"
                                id="updateAccount"
                                color="primary"
                                component={Link}
                                to={{
                                    pathname: "/Transaction/create",
                                    state: {
                                        chaincodeName: "ContractCert",
                                        chaincodeVersion: 1,
                                        chaincodeFunction: "UpdateSigner",
                                        chaincodeArgs: [`{"name":"${name}", "creditCode":"${creditCode}", "mobile":"${mobile}", "certNames":[] }`],
                                    },
                                }}
                            >
                                <Icon className={classes.buttonIcon}>person</Icon>
                                更新账号信息
                            </Button>
                            : null
                        )
                }
            </Toolbar>
        );
    }
}

export default withStyles(styles)(AccountEditToolbar);
