/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from "react";
import get from "lodash/get";
import PropTypes from "prop-types";
import SyntaxHighlighter from "react-syntax-highlighter/dist/esm/light";
import googlecode from "react-syntax-highlighter/dist/esm/styles/hljs/googlecode";
import scalaLang from "react-syntax-highlighter/dist/esm/languages/hljs/scala";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

SyntaxHighlighter.registerLanguage("scala", scalaLang);

const CodeField = ({
    source, record = {}, language, expansionSummary,
}) => <ExpansionPanel>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} >
        <Typography variant="caption" style={{ flexBasis: "33.33%" }}>{expansionSummary[0]}</Typography>
        <Typography variant="caption" style={{ flexBasis: "33.33%" }}>{expansionSummary[1]}</Typography>
        <Typography variant="caption" >{expansionSummary[2]}</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
        <SyntaxHighlighter 
            language={language}
            style={googlecode}
            showLineNumbers
        >
            {get(record, source)}
        </SyntaxHighlighter>
    </ExpansionPanelDetails>
</ExpansionPanel>;

CodeField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
    language: PropTypes.string,
    expansionSummary: PropTypes.array,
};

CodeField.defaultProps = { addLabel: true };

export default CodeField;
