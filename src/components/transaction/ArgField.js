import React from "react";
import { get } from "lodash";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = () => ({
    pre: {
        background: "rgba(0,0,0,0.1)",
        border: "1px solid #ccc",
        padding: 20,
        maxHeight: "40rem",
        overflowY: "auto",
        whiteSpace: "pre-wrap",
        wordBreak: "break-all",
    },
});

const ArgField = ({ record = {}, source, classes }) => {
    let arg = get(record, source);
    try {
        arg = JSON.parse(arg);
        arg = JSON.stringify(arg, 0, 2);
    } catch (e) {
        console.log("the transaction contract function arg is not a json, just use the raw");
    }
    return (<pre className={classes.pre}>{arg}</pre>);
};

const ArgFieldWithLabel = withStyles(styles)(ArgField);
ArgFieldWithLabel.defaultProps = {
    addLabel: true,
};

export default ArgFieldWithLabel;
