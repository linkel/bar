
/*
 * Copyright  2018 Linkel Technology Co., Ltd, Beijing
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BA SIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, { Component } from "react";
import { Responsive, Title } from "react-admin";
import KeypairIcon from "@material-ui/icons/VpnKey";
import AccountIcon from "@material-ui/icons/AccountCircle";
import TransIcon from "@material-ui/icons/Receipt";
import BlockIcon from "@material-ui/icons/ViewColumn";
import CircularProgress from "@material-ui/core/CircularProgress";
import { GraphQLClient } from "graphql-request";
import indexDataProvider from "../../dataprovider/ra-data-indexdb";
import DashItem from "./dashItem";
import settings from "../../settings";

const styles = {
    flex: { display: "flex" },
    flexColumn: { display: "flex", flexDirection: "column" },
    leftCol: { flex: 1, marginRight: "1em" },
    rightCol: { flex: 1, marginLeft: "1em" },
    singleCol: { marginTop: "1em", marginBottom: "1em", marginRight: "1em" },
    circularProgress: { width: 35, height: 35, marginRight: -3 },
};

class Dashboard extends Component {
    constructor() {
        super();
        this.state = {};
    }

    componentDidMount() {
        indexDataProvider("GET_COUNT", "accounts").then((count) => this.setState({
            accountCount: count,
        }));
        indexDataProvider("GET_COUNT", "keypairs").then((count) => this.setState({
            keypairCount: count,
        }));

        const graphqlClient = new GraphQLClient(`${settings.Prisma.endpoint}`);
        graphqlClient.request(`
            {
                transactionsConnection{
                    aggregate {
                        count
                    }
                }
            }
        `).then((res) => {
            this.setState({
                transCount: res.transactionsConnection.aggregate.count,
            });
            return 0;
        });
        graphqlClient.request(`
            {
                blocksConnection{
                    aggregate {
                        count
                    }
                }
            }
        `).then((res) => {
            this.setState({
                blockCount: res.blocksConnection.aggregate.count,
            });
            return 0;
        });
    }

    render() {
        const {
            accountCount,
            keypairCount,
            transCount,
            blockCount,
        } = this.state;
        return (
            <Responsive
                xsmall={
                    <div>
                        <Title title="pos.dashboard.welcome.title" />
                        <div style={styles.flexColumn}>
                            <div style={styles.singleCol}>
                                <DashItem
                                    icon={AccountIcon}
                                    iconBgColor="#846a06"
                                    title="pos.dashboard.account_title"
                                    value={accountCount >= 0 ? accountCount : <CircularProgress style={styles.circularProgress} />}
                                    to="accounts"
                                />   
                            </div>
                            <div style={styles.singleCol}>
                                <DashItem
                                    icon={KeypairIcon}
                                    iconBgColor="#347336"
                                    title="pos.dashboard.keypair_title"
                                    value={keypairCount >= 0 ? keypairCount : <CircularProgress style={styles.circularProgress} />}
                                    to="keypairs"
                                />
                            </div>
                            <div style={styles.singleCol}>
                                <DashItem
                                    icon={TransIcon}
                                    iconBgColor="#ff9800"
                                    title="pos.dashboard.transaction_title"
                                    value={transCount >= 0 ? transCount : <CircularProgress style={styles.circularProgress} />}
                                    to="Transaction"
                                />
                            </div>
                            <div style={styles.singleCol}>
                                <DashItem
                                    icon={BlockIcon}
                                    iconBgColor="#f44336"
                                    title="pos.dashboard.block_title"
                                    value={blockCount >= 0 ? blockCount : <CircularProgress style={styles.circularProgress} />}
                                    to="Block"
                                />
                            </div>
                            
                        </div>
                    </div>
                }
                medium={
                    <div style={styles.flexColumn}>
                        <Title title="pos.dashboard.welcome.title" />
                        <div style={styles.flex}>
                            <DashItem
                                icon={AccountIcon}
                                iconBgColor="#846a06"
                                title="pos.dashboard.account_title"
                                value={accountCount >= 0 ? accountCount : <CircularProgress style={styles.circularProgress} />}
                                to="accounts"
                            />
                            <DashItem
                                icon={KeypairIcon}
                                iconBgColor="#347336"
                                title="pos.dashboard.keypair_title"
                                value={keypairCount >= 0 ? keypairCount : <CircularProgress style={styles.circularProgress} />}
                                to="keypairs"
                            />
                            <DashItem
                                icon={TransIcon}
                                iconBgColor="#ff9800"
                                title="pos.dashboard.transaction_title"
                                value={transCount >= 0 ? transCount : <CircularProgress style={styles.circularProgress} />}
                                to="Transaction"
                            />
                            <DashItem
                                icon={BlockIcon}
                                iconBgColor="#f44336"
                                title="pos.dashboard.block_title"
                                value={blockCount >= 0 ? blockCount : <CircularProgress style={styles.circularProgress} />}
                                to="Block"
                            />
                        </div>
                    </div>
                }
            />
        );
    }
}

export default Dashboard;
